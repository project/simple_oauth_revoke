# Simple OAuth Revocation Endpoint

This module implements the token revocation endpoint for OAuth 2.0 as
outlined by [RFC 7009](https://datatracker.ietf.org/doc/html/rfc7009).

After enabling this module, an `/oauth/revoke` endpoint will be available to
revoke access or refresh tokens previously obtained by the Simple OAuth module.

## Revoking a Token
To revoke a token, a `POST` request can be made to `/oauth/revoke`.
The body of the request must be in the `application/x-www-form-urlencoded`
format and contain a `token` parameter set to the token to revoke.

### Authorization
The request must be authorized with the client that originally
issued the tokens. The `client_id` and `client_secret` can be provided in
the request body or via `Basic` authentication.

Alternatively, a bearer token may be used to authorize the request.

### Example

    curl --location 'https://example.com/oauth/revoke' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data-urlencode 'token=<access or refresh token>' \
    --data-urlencode 'client_id=<client id>' \
    --data-urlencode 'client_secret=<client secret>'

For more details, see the [RFC 7009](https://datatracker.ietf.org/doc/html/rfc7009)
specification.

## Issues and contributions

Issues and development happens in the
[Drupal.org issue queue](https://www.drupal.org/project/issues/simple_oauth_revoke).
