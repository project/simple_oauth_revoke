<?php

namespace Drupal\simple_oauth_revoke\Controller;

use Defuse\Crypto\Core;
use Defuse\Crypto\Crypto;
use Drupal\consumers\Entity\ConsumerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Utility\Error;
use Drupal\simple_oauth\Authentication\TokenAuthUser;
use Drupal\simple_oauth\Server\ResourceServerInterface;
use GuzzleHttp\Psr7\Response;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Psr\Log\LogLevel;
use Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Implements token revocation per RFC 7009.
 *
 * @see https://datatracker.ietf.org/doc/html/rfc7009
 */
class Oauth2Revoke extends ControllerBase {
  /**
   * The access token repository.
   *
   * @var \League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface
   */
  protected $accessTokenRepository;

  /**
   * The refresh token repository.
   *
   * @var \League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface
   */
  protected $refreshTokenRepository;

  /**
   * The client repository.
   *
   * @var \GuzzleHttp\ClientRepositoryInterface
   */
  protected $clientRepository;

  /**
   * The Psr message factory.
   *
   * @var \Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface
   */
  protected $messageFactory;

  /**
   * The OAuth resource server.
   *
   * @var \Drupal\simple_oauth\Server\ResourceServerInterface
   */
  protected $resourceServer;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Oauth2Revoke constructor.
   *
   * @param \League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface $access_token_repository
   *   The access token repository.
   * @param \League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface $refresh_token_repository
   *   The refresh token repository.
   * @param \GuzzleHttp\ClientRepositoryInterface $client_repository
   *   The OAuth client repository.
   * @param \Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface $message_factory
   *   The HTTP message factory.
   * @param \Drupal\simple_oauth\Server\ResourceServerInterface $resource_server
   *   The OAuth resource server.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(
    AccessTokenRepositoryInterface $access_token_repository,
    RefreshTokenRepositoryInterface $refresh_token_repository,
    ClientRepositoryInterface $client_repository,
    HttpMessageFactoryInterface $message_factory,
    ResourceServerInterface $resource_server,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    $this->accessTokenRepository = $access_token_repository;
    $this->refreshTokenRepository = $refresh_token_repository;
    $this->clientRepository = $client_repository;
    $this->messageFactory = $message_factory;
    $this->resourceServer = $resource_server;
    $this->logger = $logger_factory->get('simple_oauth_revoke');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_oauth.repositories.access_token'),
      $container->get('simple_oauth.repositories.refresh_token'),
      $container->get('simple_oauth.repositories.client'),
      $container->get('psr7.http_message_factory'),
      $container->get('simple_oauth.server.resource_server'),
      $container->get('logger.factory')
    );
  }

  /**
   * Processes POST requests to /oauth/revoke.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The revoke request object.
   */
  public function revoke(Request $request) {
    try {
      $consumer = $this->validateClient($request);

      if (!$request->get('token')) {
        throw OAuthServerException::invalidRequest('token');
      }

      // The revocation request could be for an access or a refresh token.
      $this->revokeAccessToken($request, $consumer);
      $this->revokeRefreshToken($request, $consumer);

      // The spec requires that we return a 200 status code if the token
      // was successfully revoked or if it was an invalid token.
      return new Response();
    }
    catch (OAuthServerException $exception) {
      Error::logException($this->logger, $exception, Error::DEFAULT_ERROR_MESSAGE, [], LogLevel::NOTICE);
      $response = $exception->generateHttpResponse(new Response());
    }
    return $response;
  }

  /**
   * Validates the client making the request to revoke a token.
   *
   * Client credentials may be in the Authorization header
   * as Basic authentication or in the POST request body.
   *
   * For convenience, we'll also allow a valid access token.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request being validated.
   *
   * @return \Drupal\consumers\Entity\ConsumerInterface
   *   The Drupal consumer.
   *
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   *   Thrown if the client could not be validated.
   */
  protected function validateClient(Request $request): ConsumerInterface {
    $server_request = $this->messageFactory->createRequest($request);

    // We are avoiding using null-coallesing or similar tricks to make the
    // code more elegant because we need to ensure that the secret used
    // came from the same place where we got the client ID.
    // First, attempt to get client credentials from the basic auth header.
    $client_id = $request->getUser();
    $client_secret = $request->getPassword();

    if (empty($client_id) || !$this->clientRepository->getClientEntity($client_id)) {
      // If there were no credentials, or they didn't map to a client,
      // check the body of the request for the credentials.
      $client_id = $request->get('client_id');
      $client_secret = $request->get('client_secret');
    }

    /** @var \Drupal\simple_oauth\Entities\ClientEntityInterface $client */
    if (!empty($client_id) && ($client = $this->clientRepository->getClientEntity($client_id))) {
      if (!$this->clientRepository->validateClient($client_id, $client_secret, NULL)) {
        throw OAuthServerException::invalidClient($server_request);
      }

      return $client->getDrupalEntity();
    }

    // If no client_id was set on the request; check to see if the request
    // was already authenticated by OAuth and which consumer authorized it.
    $user = $this->currentUser();

    if ($user instanceof AccountProxyInterface && $user->getAccount() instanceof TokenAuthUser) {
      /** @var \Drupal\simple_oauth\Authentication\TokenAuthUser $token */
      $token = $user->getAccount();
      return $token->getConsumer();
    }

    throw OAuthServerException::invalidClient($server_request);
  }

  /**
   * Revokes the token if the revocation request was for an access token.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The revocation request.
   * @param \Drupal\consumers\Entity\ConsumerInterface $consumer
   *   The client authorizing the request.
   *
   * @return bool
   *   Returns TRUE if a token was revoked.
   *
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   *   Thrown if the token was not issued by the client for this request.
   */
  protected function revokeAccessToken(Request $request, ConsumerInterface $consumer): bool {
    $token = $request->get('token');

    // The PHP League framework makes it very challenging to manually
    // authorize an access token. The safest thing to do is to pretend
    // like we're making a request with the access token and then use
    // the token attributes that get appended to the request object.
    // @see https://github.com/thephpleague/oauth2-server/issues/806
    $fake_request = clone $request;
    $fake_request->headers->set('Authorization', "Bearer $token");

    try {
      $authenticated_request = $this->resourceServer->validateAuthenticatedRequest($fake_request);
    }
    catch (OAuthServerException $e) {
      // Request must have not been for an access token.
      return FALSE;
    }

    $client_id = $authenticated_request->attributes->get('oauth_client_id');
    if ($client_id != $consumer->getClientId()) {
      throw OAuthServerException::invalidRequest('token', 'Mismatched client ID');
    }

    $access_token_id = $authenticated_request->attributes->get('oauth_access_token_id');
    $this->accessTokenRepository->revokeAccessToken($access_token_id);

    return TRUE;
  }

  /**
   * Revokes the token if the revocation request was for a refresh token.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The revocation request.
   * @param \Drupal\consumers\Entity\ConsumerInterface $consumer
   *   The client authorizing the request.
   *
   * @return bool
   *   Returns TRUE if a token was revoked.
   *
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   *   Thrown if the token was not issued by the client for this request.
   */
  protected function revokeRefreshToken(Request $request, ConsumerInterface $consumer): bool {
    $token = $request->get('token');

    // This must be the same encryption key used
    // by `Oauth2GrantManager::getAuthorizationServer`.
    $salt = Settings::getHashSalt();
    $encryption_key = Core::ourSubstr($salt, 0, 32);

    try {
      $jwt = Crypto::decryptWithPassword($token, $encryption_key);
    }
    catch (\Throwable $t) {
      // Request must have not been for a refresh token.
      return FALSE;
    }

    $attributes = json_decode($jwt, TRUE);

    if (!isset($attributes['client_id']) || $attributes['client_id'] != $consumer->getClientId()) {
      throw OAuthServerException::invalidRequest('token', 'Mismatched client ID');
    }

    $this->refreshTokenRepository->revokeRefreshToken($attributes['refresh_token_id']);

    if (isset($attributes['access_token_id'])) {
      $this->accessTokenRepository->revokeAccessToken($attributes['access_token_id']);
    }

    return TRUE;
  }

}
